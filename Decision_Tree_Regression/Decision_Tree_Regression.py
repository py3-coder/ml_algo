#!/usr/bin/env python
# coding: utf-8

# ## Author : Saurabh Kumar

# In[2]:


pwd


# In[3]:


path='E:\\DataScience\\Machine_Leraning_Algo\\Decision_Tree_Regression'


# In[4]:


import os
os.listdir()


# ## Decision Tree Regression

# Decision Tree is one of the most commonly used, practical approaches for supervised learning. It can be used to solve both Regression and Classification tasks with the latter being put more into practical application.
# 
# It is a tree-structured classifier with three types of nodes. The Root Node is the initial node which represents the entire sample and may get split further into further nodes. The Interior Nodes represent the features of a data set and the branches represent the decision rules. Finally, the Leaf Nodes represent the outcome. This algorithm is very useful for solving decision-related problems.
# 
# ![image.png](attachment:image.png)
# 
# 
# With a particular data point, it is run completely through the entirely tree by answering True/False questions till it reaches the leaf node. The final prediction is the average of the value of the dependent variable in that particular leaf node. Through multiple iterations, the Tree is able to predict a proper value for the data point.

# ## Step 1: Importing the libraries

# In[5]:


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


# ## Step 2: Importing the dataset

# In[6]:


# Importing the dataset
dataset = pd.read_csv(path+'\\Position_Salaries.csv')


# In[7]:


dataset.head()


# In[8]:


dataset.describe()


# In[9]:


dataset.info()


# In[10]:


dataset.isnull().sum()


# In[23]:


#Independent and dependent varibale
X =dataset['Level'].values
y =dataset['Salary'].values


# In[24]:


X


# ## Step 3: Splitting the dataset into the Training set and Test set

# In[25]:


# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)


# In[26]:


#shape
print("Shape X_train :",X_train.shape)
print("Shape y_train :",y_train.shape)
print("Shape X_test :",X_test.shape)
print("Shape y_test :",y_test.shape)


# In[29]:


"""# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
sc_y = StandardScaler()
y_train = sc_y.fit_transform(y_train.reshape(-1,1))"""


# ## Step 4: Training the Decision Tree Regression model on the training set

# In[30]:


# Fitting Decision Tree Regression to the dataset
from sklearn.tree import DecisionTreeRegressor
regressor = DecisionTreeRegressor()
regressor.fit(X_train.reshape(-1,1), y_train.reshape(-1,1))


# ## Step 5: Predicting the Results

# In[31]:


y_pred = regressor.predict(X_test.reshape(-1,1))


# ## Step 6: Visualising the Decision Tree Regression Results

# In[34]:


# Visualising the Decision Tree Regression Results 
X_grid = np.arange(min(X), max(X), 0.01)
X_grid = X_grid.reshape((len(X_grid), 1))
plt.scatter(X_test, y_test, color = 'red')
plt.scatter(X_test, y_pred, color = 'green')
plt.title('Decision Tree Regression')
plt.xlabel('Level')
plt.ylabel('Salary')
plt.show()

plt.plot(X_grid, regressor.predict(X_grid), color = 'black')
plt.title('Decision Tree Regression')
plt.xlabel('Level')
plt.ylabel('Salary')
plt.show()


# In[32]:


# Visualising the Decision Tree Regression results (higher resolution)
X_grid = np.arange(min(X), max(X), 0.01)
X_grid = X_grid.reshape((len(X_grid), 1))
plt.scatter(X, y, color = 'red')
plt.plot(X_grid, regressor.predict(X_grid), color = 'blue')
plt.title('Truth or Bluff (Decision Tree Regression)')
plt.xlabel('Position level')
plt.ylabel('Salary')
plt.show()


# In[ ]:




